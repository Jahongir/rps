from django.shortcuts import render, get_object_or_404

from game.models import Game


def get_game(request, game_id):
    game = get_object_or_404(Game, pk=game_id)
    return render(request, 'game/game.html', {'game': game})
