from django.conf.urls import url

from game.api.views import GameList, get_game_history

game_urls = [
    url(r'^$', GameList.as_view(), name='game-list'),
    url(r'^history/$', get_game_history, name='game-history'),
]
