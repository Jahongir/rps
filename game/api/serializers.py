from rest_framework import serializers

from game.models import Game


class GameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ('pk', 'name', 'status', 'duration', 'user_one', 'user_two')
