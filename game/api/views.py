from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from game.api.serializers import GameSerializer
from game.models import Game, Round, Move


class GameList(generics.ListCreateAPIView):
    queryset = Game.objects.filter().exclude(status='over')
    serializer_class = GameSerializer


@api_view(('GET',))
def get_game_history(request):
    try:
        game_id = request.GET['game_id']
        game = Game.objects.get(pk=game_id)
    except KeyError:
        return Response('Wrong parameter', status=status.HTTP_400_BAD_REQUEST)
    except Game.DoesNotExist:
        return Response('Wrong ID', status=status.HTTP_400_BAD_REQUEST)

    rounds = Round.objects.filter(game=game)

    history = dict()

    for round in rounds:
        moves = Move.objects.filter(round=round).exclude(type='hint')
        user_one_move = moves.filter(user=game.user_one).first()
        user_two_move = moves.filter(user=game.user_two).first()

        if user_one_move and user_two_move:
            history[round.order] = user_one_move.type + ' vs ' + user_two_move.type
        else:
            history[round.order] = round.winner.username + ' automatically won'

        hints = Move.objects.filter(round=round, type='hint')

        for hint in hints:
            history[round.order] += ' and hint by ' + hint.user.username

    return Response(history, status=status.HTTP_200_OK)
