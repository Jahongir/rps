from django import forms


class GameCreationForm(forms.Form):
    name = forms.CharField(label='Name', max_length=255)
