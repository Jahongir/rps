from socketio.namespace import BaseNamespace
from socketio.sdjango import namespace

from django.utils import timezone

from contrib.models import ROUNDS
from contrib.views import emit_all, emit, get_socket_by, get_other_user, \
    set_game_winner, get_hint
from game.models import Game, Round, Move


@namespace('/game')
class GameNamespace(BaseNamespace):
    def initialize(self):
        self.request.user.socket_session_id = self.socket.sessid
        self.request.user.last_active = timezone.now()
        self.request.user.save()

    def on_msg(self, msg):
        emit_all(self.socket.server.sockets.iteritems(),
                 'Someone said: {0}'.format(msg), 'msg', self.ns_name)

    def on_join(self, game_id):
        current_game = Game.objects.get(pk=game_id)
        first_user = current_game.user_one
        second_user = current_game.user_two

        if first_user.id == self.request.user.id and second_user is None:
            emit(self.socket, 'Waiting for the second player', 'msg',
                 self.ns_name)

        elif first_user.id != self.request.user.id and second_user is None:
            current_game.user_two = self.request.user
            second_user = current_game.user_two
            current_game.status = 'ready'
            current_game.save()

            emit(self.socket,
                 {'msg': 'Thanks for joining. Please, wait for ' + first_user.username + ' to start',
                  'first_user': first_user.username,
                  'second_user': second_user.username},
                 'second_joined', self.ns_name)

            first_user_socket = get_socket_by(
                first_user.socket_session_id,
                self.socket.server.sockets.iteritems())

            emit(first_user_socket,
                 {'msg': second_user.username + ' joined! You can start now',
                  'first_user': first_user.username,
                  'second_user': second_user.username},
                 'ready', self.ns_name)
        else:
            emit(
                self.socket, 'Two players already joined the game!'
                ' Please choose another one!', 'msg', self.ns_name)

    def on_start(self, game_id):
        current_game = Game.objects.get(pk=game_id)
        current_game.status = 'playing'
        current_game.save()

        second_user = current_game.user_two
        second_user_socket = get_socket_by(
            second_user.socket_session_id,
            self.socket.server.sockets.iteritems())

        Round.objects.create(game=current_game, order=1)

        emit(self.socket, 'Started', 'start', self.ns_name)
        emit(second_user_socket, 'Started', 'start', self.ns_name)

    def on_turn(self, game_id, round_order, turn_type):
        game = Game.objects.get(pk=game_id)

        first_user = game.user_one
        second_user = game.user_two

        first_user_socket = get_socket_by(
            first_user.socket_session_id,
            self.socket.server.sockets.iteritems())

        second_user_socket = get_socket_by(
            second_user.socket_session_id,
            self.socket.server.sockets.iteritems())

        round_ = Round.objects.get(game=game, order=round_order)

        Move.objects.create(
            user=self.request.user, type=turn_type, round=round_)

        if Move.objects.filter(
                round__order=round_order, round__game=game).exclude(type='hint').count() == 2:
            first_user_move = Move.objects.filter(
                user=game.user_one, round=round_).exclude(type='hint').first()
            second_user_move = Move.objects.filter(
                user=game.user_two, round=round_).exclude(type='hint').first()

            first_user_result = ROUNDS[first_user_move.type][second_user_move.type]

            if first_user_result == "WIN":
                emit(first_user_socket,
                     {'msg': 'You win: ' + first_user_move.type + ' vs ' + second_user_move.type,
                      'first_result': 1, 'second_result': 0}, 'round_end',
                     self.ns_name)
                emit(second_user_socket,
                     {'msg': 'You lose: ' + first_user_move.type + ' vs ' + second_user_move.type,
                      'first_result': 1, 'second_result': 0}, 'round_end',
                     self.ns_name)

                round_.winner = first_user
            elif first_user_result == "LOSE":
                emit(first_user_socket,
                     {'msg': 'You lose: ' + first_user_move.type + ' vs ' + second_user_move.type,
                      'first_result': 0, 'second_result': 1}, 'round_end',
                     self.ns_name)
                emit(second_user_socket,
                     {'msg': 'You win: ' + first_user_move.type + ' vs ' + second_user_move.type,
                      'first_result': 0, 'second_result': 1}, 'round_end',
                     self.ns_name)
                round_.winner = second_user
            else:
                emit(first_user_socket,
                     {'msg': 'TIE!: ' + first_user_move.type + ' vs ' + second_user_move.type,
                      'first_result': 1, 'second_result': 1}, 'round_end',
                     self.ns_name)
                emit(second_user_socket,
                     {'msg': 'TIE!: ' + first_user_move.type + ' vs ' + second_user_move.type,
                      'first_result': 1, 'second_result': 1}, 'round_end',
                     self.ns_name)

            round_.save()

            if round_order != 3:
                Round.objects.create(game=game, order=round_order+1)
            else:
                set_game_winner(game.id)
        else:
            if self.request.user.id == first_user.id:
                emit(first_user_socket, 'Please wait for your opponent',
                     'msg', self.ns_name)
                emit(second_user_socket,
                     'Your opponent made his move and waiting for you!',
                     'msg', self.ns_name)
            else:
                emit(first_user_socket,
                     'Your opponent made his move and waiting for you!',
                     'msg', self.ns_name)
                emit(second_user_socket,
                     'Please wait for your opponent', 'msg', self.ns_name)

    def on_hint(self, game_id, round_):
        game = Game.objects.get(pk=game_id)
        round_ = Round.objects.get(game=game_id, order=round_)
        user_socket = get_socket_by(self.request.user.socket_session_id,
                                    self.socket.server.sockets.iteritems())

        hint_given = Move.objects.filter(
            round__game=game, user=self.request.user, type='hint')

        if hint_given:
            emit(
                user_socket, 'You already used your hint in this game',
                'msg', self.ns_name)
        else:
            Move.objects.create(
                user=self.request.user, type='hint', round=round_)

            another_user_move = Move.objects.filter(round=round_).exclude(
                type='hint', user=self.request.user).first()

            if another_user_move:
                hinted_moves = get_hint(another_user_move.type)
                emit(user_socket,
                     'Your opponent chose ' + ' or '.join(hinted_moves),
                     'msg', self.ns_name)
            else:
                emit(user_socket,
                     'Your opponent has not chosen yet! You cannot use'
                     ' hint anymore',
                     'msg', self.ns_name)

    def on_auto_end(self, game_id, round_order):
        game = Game.objects.get(pk=game_id)
        round_ = Round.objects.get(game=game, order=round_order)

        if game.user_one == self.request.user:
            first_result = 0
            second_result = 1
            other_user = game.user_two
        else:
            first_result = 1
            second_result = 0
            other_user = game.user_one

        emit(self.socket,
             {'msg': 'You ran out of time and automatically lose this round',
              'first_result': first_result, 'second_result': second_result},
             'auto_end', self.ns_name)

        other_user_socket = get_socket_by(
            other_user.socket_session_id,
            self.socket.server.sockets.iteritems())

        emit(other_user_socket,
             {'msg': 'You automatically win this round as your opponent could not make it on time',
              'first_result': first_result, 'second_result': second_result},
             'auto_end', self.ns_name)

        round_.winner = other_user
        round_.save()

        if round_order != 3:
            Round.objects.create(game=game, order=round_order + 1)
        else:
            set_game_winner(game.id)

    def on_disconnect(self, game_id):
        game = Game.objects.get(pk=game_id)
        other_user = get_other_user(game, self.request.user)
        other_user_socket = get_socket_by(
            other_user.socket_session_id,
            self.socket.server.sockets.iteritems())

        game.winner = other_user
        game.save()
        emit(other_user_socket, 'Opponent left! You WIN!', 'auto_win',
             self.ns_name)


@namespace('/main')
class MainNamespace(BaseNamespace):
    def initialize(self):
        self.request.user.last_active = timezone.now()
        self.request.user.save()

    def on_games_update(self):
        emit_all(self.socket.server.sockets.iteritems(), 'games update',
                 'games_update', self.ns_name)

