from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.db import models

from accounts.models import User

GAME_STATUSES = (
    ('waiting', 'WAITING'),
    ('ready', 'READY'),
    ('playing', 'PLAYING'),
    ('over', 'OVER'),
)

ROUND_ORDERS = (
    (1, 1),
    (2, 2),
    (3, 3)
)

MOVE_TYPES = (
    ('rock', 'ROCK'),
    ('paper', 'PAPER'),
    ('scissors', 'SCISSORS'),
    ('lizard', 'LIZARD'),
    ('spock', 'SPOCK'),
    ('hint', 'HINT')
)


class Game(models.Model):
    """
    Represents a game/lobby between two players
    """
    name = models.CharField(_('name'), max_length=255)
    status = models.CharField(
        _('status'), max_length=20, choices=GAME_STATUSES)
    duration = models.IntegerField(_('duration'), default=5)
    user_one = models.ForeignKey(User, verbose_name=_('user one'),
                                 related_name='games', blank=True, null=True)
    user_two = models.ForeignKey(User, verbose_name=_('user two'), blank=True,
                                 null=True)
    winner = models.ForeignKey(User, verbose_name=_('winner'),
                               blank=True, null=True, related_name='won_games')

    class Meta:
        verbose_name = _('game')
        verbose_name_plural = _('games')

    def __unicode__(self):
        if self.user_two:
            return self.user_one.username + ' vs ' + self.user_two.username
        else:
            return self.user_one.username + ' vs `waiting`'


class Round(models.Model):
    """
    Represents a round of the game
    """
    game = models.ForeignKey(
        Game, verbose_name=_('game'), related_name='rounds')
    winner = models.ForeignKey(User, verbose_name=_('winner'),
                               related_name='rounds', blank=True, null=True)
    order = models.IntegerField(_('order'), choices=ROUND_ORDERS)

    class Meta:
        verbose_name = _('round')
        verbose_name_plural = _('rounds')

    def __unicode__(self):
        return str(self.order) + ' round in game ' + self.game.name


class Move(models.Model):
    """
    Represents a move by some user in a round
    """
    type = models.CharField(_('type'), max_length=15, choices=MOVE_TYPES)
    user = models.ForeignKey(
        User, verbose_name=_('user'), related_name='moves')
    round = models.ForeignKey(
        Round, verbose_name=_('round'), related_name='moves')

    class Meta:
        verbose_name = _('move')
        verbose_name_plural = _('moves')

    def __unicode__(self):
        return self.type + ' by ' + self.user.username
