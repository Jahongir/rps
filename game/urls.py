from django.conf.urls import url

urlpatterns = [
    url(r'^(?P<game_id>[0-9]+)/$', 'game.views.get_game', name='get-game'),
]
