from random import randint, shuffle

from game.models import MOVE_TYPES, Game, Round


def get_or_none(model, *args, **kwargs):
    try:
        return model.objects.get(*args, **kwargs)
    except model.DoesNotExist:
        return None


def get_socket_by(sess_id, items):
    for sessid, socket in items:
        if sessid == sess_id:
            return socket


def emit(socket, msg, msg_type, endpoint):
    pkt = dict(type='event',
               name=msg_type,
               args=msg,
               endpoint=endpoint)

    socket.send_packet(pkt)


def emit_all(items, msg, msg_type, endpoint):
    pkt = dict(type='event',
               name=msg_type,
               args=msg,
               endpoint=endpoint)

    for sessid, socket in items:
        socket.send_packet(pkt)


def get_hint(move):
    other_moves = []

    for move_tuple in MOVE_TYPES:
        if move_tuple[0] != move and move_tuple[0] != 'hint':
            other_moves.append(move_tuple[0])

    random_move = other_moves[randint(0, 3)]
    hinted_moves = [random_move, move]

    shuffle(hinted_moves)

    return hinted_moves


def set_game_winner(game_id):
    game = Game.objects.get(pk=game_id)

    first_user_wins = Round.objects.filter(winner=game.user_one).count()
    second_user_wins = Round.objects.filter(winner=game.user_two).count()

    if first_user_wins > second_user_wins:
        game.winner = game.user_one
    elif second_user_wins > first_user_wins:
        game.winner = game.user_two

    game.status = 'over'
    game.save()


def get_other_user(game, user):
    if game.user_one == user:
        return game.user_two
    else:
        return game.user_one
