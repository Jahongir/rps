# Rock, paper, scissors, lizard and spock

It is an old and good RPSLS game in a multiplayer mode! 


## Tech stack

 - Django
 - DRF
 - SQLite
 - jQuery
 - Gevent-socketio
 
## Installation

Simply do `pip install -r requirements.txt` after cloning

Then get socketio server code by `git clone https://github.com/iamjem/socketio_runserver`

Then start the project by `python manage.py socketio_runserver`

