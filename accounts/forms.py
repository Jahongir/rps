from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _


class UserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'duplicate_username': _("A user with that username already exists."),
    }
    username = forms.CharField(label=_("Username"), widget=forms.TextInput(
        attrs={'placeholder': 'Username', 'required': True,
               'class': 'form-control'}))
    password = forms.CharField(
        label=_("Password"), widget=forms.PasswordInput(
            attrs={'placeholder': 'Password', 'required': True,
                   'class': 'form-control'}))

    class Meta:
        model = get_user_model()
        fields = ("username", "password")

    def clean_username(self):
        # Since CustomUser.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            get_user_model()._default_manager.get(username=username)
        except get_user_model().DoesNotExist:
            return username
        raise forms.ValidationError(
            self.error_messages['duplicate_username'],
            code='duplicate_username',
        )

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user
