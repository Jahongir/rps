from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, \
    BaseUserManager


class CustomUserManager(BaseUserManager):
    def _create_user(self, username, password, is_staff, is_superuser,
                     **extra_fields):
        """
        Creates and saves a User with the given username and password.
        """
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        user = self.model(is_staff=is_staff, is_superuser=is_superuser,
                          last_login=now, last_active=now, date_joined=now,
                          username=username, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password=None, **extra_fields):
        return self._create_user(
            username, password, False, False, **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        return self._create_user(
            username, password, True, True, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """
    Custom User model with admin-compliant permission.
    Username and password are required. Other fields are optional.
    """
    username = models.CharField(
        _('username'), max_length=255, unique=True)
    is_staff = models.BooleanField(
        _('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    quit_at = models.DateTimeField(_('quit at'), null=True, blank=True)
    last_active = models.DateTimeField(_('last active'), null=True, blank=True)
    socket_session_id = models.CharField(
        _('socket session id'), null=True, blank=True, max_length=30)

    objects = CustomUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    def __unicode__(self):
        return self.get_full_name()

