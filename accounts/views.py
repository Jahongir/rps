from django.contrib import auth
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render

from accounts.forms import UserCreationForm


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            user = auth.authenticate(username=request.POST['username'],
                                     password=request.POST['password'])
            auth.login(request, user)
            return HttpResponseRedirect(reverse('index'))
    else:
        form = UserCreationForm()

    return render(request, 'accounts/register.html', {'form': form})


def login(request):
    context = dict()

    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        context['next'] = request.POST.get('next', '/')

        user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            return HttpResponseRedirect(context['next'])
        else:
            context['error'] = 'Incorrect username or password'
    else:
        context['next'] = request.GET.get('next', '/')

    return render(request, 'accounts/login.html', context)
