from django.conf.urls import url

urlpatterns = [
    url(r'^register', 'accounts.views.register', name='register'),
    url(r'^login', 'accounts.views.login', name='login'),
]
