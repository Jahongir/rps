from rest_framework import generics

from datetime import timedelta
from django.utils import timezone

from accounts.api.serializer import UserSerializer
from accounts.models import User


class UserList(generics.ListCreateAPIView):
    last_ten_minutes = timezone.now() - timedelta(minutes=10)
    queryset = User.objects.filter(last_active__gte=last_ten_minutes)
    serializer_class = UserSerializer
