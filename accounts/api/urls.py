from django.conf.urls import url

from accounts.api.views import UserList

account_urls = [
    url(r'^active/$', UserList.as_view(), name='user-list'),
]
