from django.conf.urls import url, include
from django.contrib import admin

from rps.api.urls import api_urls
from socketio import sdjango

sdjango.autodiscover()

urlpatterns = [
    url(r'^$', 'rps.views.index', name='index'),
    url(r'^socket\.io', include(sdjango.urls)),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('accounts.urls', namespace='accounts')),
    url(r'^games/', include('game.urls', namespace='games')),

    url(r'^api/', include(api_urls, namespace='api')),
]
