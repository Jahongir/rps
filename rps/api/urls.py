from django.conf.urls import url, include

from accounts.api.urls import account_urls
from game.api.urls import game_urls

api_urls = [
    url(r'^games/', include(game_urls, namespace='games')),
    url(r'^accounts/', include(account_urls, namespace='accounts')),
]
