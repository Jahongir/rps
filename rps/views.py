from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from game.forms import GameCreationForm


@login_required
def index(request):
    game_form = GameCreationForm()
    return render(request, 'index.html', {'form': game_form})
